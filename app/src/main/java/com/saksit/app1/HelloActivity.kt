package com.saksit.app1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView

class HelloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_hello)

        val displayName: TextView = findViewById(R.id.hello_fullname)
        val fullname: String = intent.getStringExtra("Name").toString()
        displayName.text = fullname
    }
}