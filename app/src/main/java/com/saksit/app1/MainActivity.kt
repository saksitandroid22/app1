package com.saksit.app1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val helloButton:Button = findViewById(R.id.hello_button)
        helloButton.setOnClickListener {
            val fullname:TextView = findViewById(R.id.fullname)
            val studentId:TextView = findViewById(R.id.student_id)
            Log.d(TAG, "${fullname.text} ${studentId.text}")

            val Intent = Intent(this,HelloActivity::class.java)
            Intent.putExtra("Name",fullname.text)

            startActivity(Intent)

        }
    }
}